CREATE TABLE [Eddie].[CompoundKeys]
(
[MyID] [int] NOT NULL,
[MyName] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MyDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Eddie].[CompoundKeys] ADD CONSTRAINT [PK_CompoundKeys] PRIMARY KEY CLUSTERED  ([MyID], [MyName]) ON [PRIMARY]
GO
