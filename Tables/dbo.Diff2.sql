CREATE TABLE [dbo].[Diff2]
(
[ColID] [int] NOT NULL IDENTITY(1, 1),
[address1] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address2] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
