CREATE TABLE [Eddie].[Articles]
(
[Articles_ID] [int] NOT NULL IDENTITY(1, 1),
[ArticleName] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArticleSubject] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArticleLocation] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArticleAuthor] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Author_ID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Eddie].[Articles] ADD CONSTRAINT [prim_Articles] PRIMARY KEY CLUSTERED  ([Articles_ID]) ON [PRIMARY]
GO
ALTER TABLE [Eddie].[Articles] ADD CONSTRAINT [foreign_Author] FOREIGN KEY ([Author_ID]) REFERENCES [Eddie].[Author] ([Author_ID])
GO
