CREATE TABLE [dbo].[Job_Roles]
(
[JobID] [int] NOT NULL,
[Job_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Job_Roles] ADD CONSTRAINT [pk_Job_Roles] PRIMARY KEY NONCLUSTERED  ([JobID]) ON [PRIMARY]
GO
