CREATE TABLE [UCBTest].[SelectiveAccessRule]
(
[SelectiveAccessRuleId] [uniqueidentifier] NOT NULL,
[SelectiveAccessGroupId] [uniqueidentifier] NOT NULL,
[TagDefId] [uniqueidentifier] NOT NULL,
[LovPossibleValueId] [uniqueidentifier] NOT NULL,
[RowCreatedOn] [datetime] NULL CONSTRAINT [DF__Selective__RowCr__19CACAD2] DEFAULT (sysdatetime()),
[RowLastModifiedOn] [datetime] NULL CONSTRAINT [DF__Selective__RowLa__1ABEEF0B] DEFAULT (sysdatetime()),
[ID] [int] NOT NULL,
[TagValue] [sql_variant] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [UCBTest].[SelectiveAccessRule] ADD CONSTRAINT [SelectiveAccessRule_PK] PRIMARY KEY CLUSTERED  ([SelectiveAccessRuleId]) ON [PRIMARY]
GO
